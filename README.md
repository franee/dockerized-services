# Dockerized Services
Easy setup for services in your development environment.

### Services offered

| Service                            | Version | Ports |
| ---------------------------------- | ------- | ------|
| redis                              | 4.0.10  | 6379  |
| redis_persistent (aof strategy)    | 4.0.10  | 6380  |
| mariadb (root / root)              | 10.3.7  | 3306  |
| postgres (root / root)             | 10.4    | 5432  |
| mongo (root / root)                | 3.6.5   | 27017 |
| adminer (web admin for dbs)        | 4.6.3   | 8080  |

### Requirements
1. Install docker
2. Install docker-compose

### Setup

```bash
$ git clone https://gitlab.com/franee/dockerized-services.git
$ cd dockerized-services
$ docker-compose up
```

### How to go to mysql shell?

```bash
$ docker-compose exec mariadb mysql -hmariadb -P3306 -u root -p
```

### How to go to mongo shell?

```bash
docker-compose exec mongo mongo admin --host mongo --username root --password root
```

### Adminer

Access web-db-admin on localhost:8080

Available hostnames:
* mariadb
* mongo
* postgres